now=$(date +"%Y%m%d-%H%M%S")
TARGET=mongo_${DB_NAME}_${DB_HOST}_${now}
BPATH=/backups
TMPDIR=/tmp

mongodump --out ${TMPDIR}/${TARGET} --host ${DB_HOST} --port 27017 --username ${DB_USER} --password ${DB_PASS} --authenticationDatabase ${DB_NAME}

TARGET2=${TARGET}.tar.gz
tar -zcvf ${TMPDIR}/${TARGET2} ${TMPDIR}/${TARGET}

mv ${TMPDIR}/${TARGET2} ${BPATH}/${TARGET2}

find ${BPATH}/ -mmin +${DB_CLEANUP_TIME} -iname "mongo_$DBNAME_*.*" -exec rm {} \;

