# FROM ubuntu:14.04
FROM tiredofit/alpine:edge
RUN mkdir /dump
WORKDIR /dump

ADD . /dump
RUN crontab /dump/crontabfile
RUN apk update && \
      apk add \
        rsyslog \
        mongodb-tools \
        bzip2 \
        xz && \
    rm -rf /var/cache/apk/* 

RUN cp /dump/crontabfile /etc/crontab
RUN mkdir /dump/log
RUN touch /dump/log/cron.log
RUN chmod +x /dump/run.sh

CMD ["sh","/dump/run.sh"]

