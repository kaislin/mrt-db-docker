#!/usr/bin/env bash
echo "Creating mongo users..."
mongo $MONGO_INITDB_DATABASE --port 27017 -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin --eval "db.createUser({user: '$MONGO_INITDB_ROOT_USERNAME', pwd: '$MONGO_INITDB_ROOT_PASSWORD', roles: [{role: 'root', db: 'admin'},{role: 'readWrite', db: '$MONGO_INITDB_DATABASE'}]});"
echo "Mongo users created."
